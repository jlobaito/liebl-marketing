<?php
/**
 * @package contactsubmission
 * @subpackage processors
 */
class ContactSubmissionRemoveProcessor extends modObjectRemoveProcessor {
    public $classKey = 'ContactSubmission';
    public $languageTopics = array('contactsubmissions:default');
    public $objectType = 'contactsubmissions.contactsubmission';
}
return 'ContactSubmissionRemoveProcessor';