<?php
/**
 * @package contactsubmissions
 * @subpackage controllers
 */
class ContactSubmissionsHomeManagerController extends ContactSubmissionsManagerController {
    public function process(array $scriptProperties = array()) {

    }
    public function getPageTitle() { return $this->modx->lexicon('contactsubmissions'); }
    public function loadCustomCssJs() {
        $this->addJavascript($this->contactsubmissions->config['jsUrl'].'mgr/widgets/contactsubmissions.grid.js');
        $this->addJavascript($this->contactsubmissions->config['jsUrl'].'mgr/widgets/home.panel.js');
        $this->addLastJavascript($this->contactsubmissions->config['jsUrl'].'mgr/sections/index.js');
    }
    public function getTemplateFile() { return $this->contactsubmissions->config['templatesPath'].'home.tpl'; }
}