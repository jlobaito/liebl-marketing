<?

$output = "


        <script type='text/javascript' src='/core/components/help/flowplayer/flowplayer-3.2.12.min.js'></script>
        <script type='text/javascript'>
            swfobject.registerObject('csSWF', '9.0.115', 'expressInstall.swf');
        </script>
        <style type='text/css'>
            .media
            {
                margin-top: 20px;
            }
            #noUpdate
            {
                margin: 0 auto;
                font-family:Arial, Helvetica, sans-serif;
                font-size: x-small;
                color: #cccccc;
                text-align: left;
                width: 210px; 
                height: 200px;	
                padding: 40px;
            }
        </style>

<div class='container'>
<h2>Help Page...</h2>
		
		<div class='media'>
			<h4>Page Creation</h4>

            <a href='/core/components/help/AAFP Page Creation.mp4'
               style='display:block;width:425px;height:300px;'
               id='player_1'>
            </a>

            <script language='JavaScript'>
              flowplayer('player_1', '/core/components/help/flowplayer/flowplayer-3.2.16.swf', {
                clip: {
                    autoPlay: false,
                    autoBuffering: true
                }
              });
            </script>

        </div>
		
		<div class='media'>
			<h4>Site Assets</h4>

            <a href='/core/components/help/AAFP Assets.mp4'
               style='display:block;width:425px;height:300px;'
               id='player_2'>
            </a>

            <script language='JavaScript'>
              flowplayer('player_2', '/core/components/help/flowplayer/flowplayer-3.2.16.swf', {
                clip: {
                    autoPlay: false,
                    autoBuffering: true
                }
              });
            </script>
        
        </div>
        
        <div class='media'>
            <h4>Menu Links</h4>

            <a href='/core/components/help/AAFP Menu Links.mp4'
               style='display:block;width:425px;height:300px;'
               id='player_3'>
            </a>

            <script language='JavaScript'>
              flowplayer('player_3', '/core/components/help/flowplayer/flowplayer-3.2.16.swf', {
                clip: {
                    autoPlay: false,
                    autoBuffering: true
                }
              });
            </script>

        </div>		

		<div class='media'>
			<h4>Content Links</h4>

            <a href='/core/components/help/AAFP Content Links.mp4'
               style='display:block;width:425px;height:300px;'
               id='player_4'>
            </a>

            <script language='JavaScript'>
              flowplayer('player_4', '/core/components/help/flowplayer/flowplayer-3.2.16.swf', {
                clip: {
                    autoPlay: false,
                    autoBuffering: true
                }
              });
            </script>

        </div>

</div>

";

return $output;
?>